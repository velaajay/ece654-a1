"""
This is script is just to add some methods with true positive and true negative test cases
for control structure nesting
"""


# This method will have actual nesting with a level greater than or equal to 4.
def test_ctrl_struct_nesting_tp():
    count = 0
    if True:
        count += 1
        print("Level {0} reached".format(count))
        if True:
            count += 1
            print("Level {0} reached".format(count))
            if True:
                count += 1
                print("Level {0} reached".format(count))
                if True:
                    count += 1
                    print("Level {0} reached".format(count))


# This method will have control flow nesting with a level less than 4
def test_ctrl_struct_nesting_tn():
    count = 0
    if True:
        count += 1
        print("Level {0} reached".format(count))
        if True:
            count += 1
            print("Level {0} reached".format(count))
            if True:
                count += 1
                print("Level {0} reached".format(count))




def test_ctrl_struct_nesting_fp():
    count = 0
    if True:
        count += 1
        print("Level {0} reached".format(count))
        if True:
            count += 1
            print("Level {0} reached".format(count))
            if True:
                count += 1
                print("Level {0} reached".format(count))
            if True:
                count += 1
                print("Level {0} reached".format(count))


def test_ctrl_struct_nesting_with_elif(input_count):
    count = input_count
    if count > 0:
        count += 1
        print("Level {0} reached".format(count))
    elif True:
        count += 1
        print("Level {0} reached".format(count))
        if True:
            count += 1
            print("Level {0} reached".format(count))
            if True:
                count += 1
                print("Level {0} reached".format(count))
                if True:
                    count += 1
                    print("Level {0} reached".format(count))
                    if True:
                        count += 1
                        print("Level {0} reached".format(count))
