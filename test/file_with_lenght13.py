"""
The purpose of this file is to place identifiers with length 13
"""


def hello_static_analysis():
    vlen_thirteen = "Exact 13"
    print("This is print statement {}".format(vlen_thirteen))
    vlen_fourteen1 = "Exact 14"
    print("This is print statement {}".format(vlen_fourteen1))