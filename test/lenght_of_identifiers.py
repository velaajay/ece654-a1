"""
This file we will add test scenarios for identifier length
"""

from sys import getallocatedblocks
import numpy as np

global this_is_global
this_is_global = 'I am a global variable'

nonlocal this_is_nonlocal
this_is_nonlocal = 'I am a nonlocal variable'


class SampleClass:
    def __init__(self):
        self.class_attribute = "I am just a class attribute"


def student_info(input_variable):
    first = "Ajay"
    last = "Kumar"
    watiam = "a289kuma"
    student = 20875438
    email = "a289kumar@uwaterloo.ca"
    hours = "35"
    map_variable_to_input = input_variable
    print(
        "First {0}\nLast {1}\nWatiam {2}\nStudent {3}\nEmail {4}\nHours{5}".format(first, last, watiam, student, email,
                                                                                   hours))


def greet_me(**kwargs):
    for key, value in kwargs.items():
        print("{0} = {1}".format(key, value))





