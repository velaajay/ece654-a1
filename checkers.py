import ast
import tokenize
from prettytable import PrettyTable
import sys


def read_tokens(input_file):
    with tokenize.open(input_file) as fd:
        return fd.read()


class BaseChecker(ast.NodeVisitor):
    def __init__(self):
        self.errors = list()
        self.filename = None

    def check(self, paths):
        for filepath in paths:
            self.filename = filepath
            tree = ast.parse(read_tokens(filepath))
            self.visit(tree)

    def report(self):
        table = PrettyTable(border=True, header=True, padding_width=1,
                            field_names=["File name", "Line number", "Error Type"])
        for error in self.errors:
            if error is not None:
                table.add_row(error)

        print(table)


class TooManyControlStatement(BaseChecker):
    def __init__(self):
        super().__init__()
        self.current_if_depth = 0
        self.msg = "Maximum control structure nesting of 4 reached"

    def visit_If(self, node, parent=True):
        if parent:
            self.current_if_depth = 1
        else:
            self.current_if_depth += 1

        for child in node.body:
            if type(child) == ast.If:
                self.visit_If(child, parent=False)

        if parent and self.current_if_depth >= 4:
            self.errors.append((self.filename, node.lineno, self.msg))
            self.current_if_depth = 0
            return

        # Lets traverse the nodes of Elseif block.
        # Changes the level back to 1 because we are going in else block.
        if parent:
            self.current_if_depth = 1

        for child in node.orelse:
            for elif_child in child.body:
                if type(elif_child) == ast.If:
                    self.visit_If(elif_child, parent=False)

        if parent and self.current_if_depth >= 4:
            self.errors.append((self.filename, node.lineno, self.msg))
            self.current_if_depth = 0
            return


class IdentifierLengthChecker(BaseChecker):
    def __init__(self):
        super().__init__()
        self.msg = "Found identifier with length exactly 13"
        self.identifier_size_map = dict()

    def visit_Name(self, node):
        self.handler(node.id)

    def visit_ClassDef(self, node):
        self.handler(node.name)

    def visit_ImportFrom(self, node):
        self.handler(node.module)

    def visit_Global(self, node):
        for name in node.names:
            self.handler(name)

    def visit_Nonlocal(self, node):
        for name in node.names:
            self.handler(name)

    def visit_Attribute(self, node):
        self.handler(node.attr)

    def visit_arg(self, node):
        self.handler(node.arg)

    def visit_keyword(self, node):
        self.handler(node.arg)

    def visit_alias(self, node):
        self.handler(node.asname)

    def handler(self, identifier):
        # print(identifier)
        if identifier is not None:
            self.identifier_size_map[identifier] = self.filename

    def report(self):
        table = PrettyTable(border=True, header=True, padding_width=1,
                            field_names=["File name", "identifier name", "identifier size", "OK/NOK"])
        for k, v in self.identifier_size_map.items():
            table.add_row([v, k, len(k), "OK" if len(k) != 13 else "NOK"])
        print(table, file=sys.stdout)
