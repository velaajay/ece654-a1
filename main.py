from checkers import IdentifierLengthChecker, TooManyControlStatement
import glob


def get_files_for_analysis(input_directory="test"):
    pattern = input_directory + "/*.py"
    result = glob.glob(pattern)
    return result


if __name__ == '__main__':
    files = get_files_for_analysis()
    ctrl_stmt_check = TooManyControlStatement()
    identifier_len_check = IdentifierLengthChecker()

    ctrl_stmt_check.check(files)
    print(
        "\n###############################   Reporting Control Stmt info  ##############################\n")
    ctrl_stmt_check.report()
    identifier_len_check.check(files)
    print(
        "\n###############################   Reporting identifiers info  ###############################\n")
    identifier_len_check.report()
